<?php

App::uses('AppController','Controller');
class ProductsController extends AppController{

	public function add(){
	    $this->layout = false;
	    $response = array('status'=>'failed', 'message'=>'HTTP method not allowed');
		if($this->request->is('post')){

	        $data = $this->request->input('json_decode', true);
	        if(empty($data)){
	            $data = $this->request->data;
	        }
	        
	        $response = array('status'=>'failed', 'message'=>'Please provide form data');
	            
	        if(!empty($data)){
	            if($this->Product->save($data)){
	                $response = array('status'=>'success','message'=>'Product successfully created');
	            } else{
	                $response = array('status'=>'failed', 'message'=>'Failed to save data');
	            }
	        }
	    }
	        
	    $this->response->type('application/json');
	    $this->response->body(json_encode($response));
	    return $this->response->send();
	}

	public function view($id = null){
	    $this->layout = false;
	    $response = array('status'=>'failed', 'message'=>'Failed to process request');

	    if(!empty($id)){
	        $result = $this->Product->findById($id);
	        if(!empty($result)){
	            $response = array('status'=>'success','data'=>$result);  
	        } else {
	            $response['message'] = 'Found no matching data';
	        }  
	    } else {
	        //$response['message'] = "Please provide ID";
	        $result = $this->Product->find('all');
	        if(!empty($result)){
	            $response = array('status'=>'success','data'=>$result);  
	        } else {
	            $response['message'] = 'Found no matching data';
	        }
	    }
	        
	    $this->response->type('application/json');
	    $this->response->body(json_encode($response));
	    return $this->response->send();
	}

	public function update($id){
	    $this->layout = false;
	    $response = array('status'=>'failed', 'message'=>'HTTP method not allowed');
	    
	    if($this->request->is('put')){
	        $data = $this->request->input('json_decode', true);
	        if(empty($data)){
	            $data = $this->request->data;
	            $id = $data['id'];
	        }
	        
	        if(!empty($id)){
	            $this->Product->id = $id;
	            if($this->Product->save($data)){
	                $response = array('status'=>'success','message'=>'Product successfully updated');
	            } else {
	                $response['message'] = "Failed to update product";
	            }
	        } else {
	            $response['message'] = 'Please provide product ID';
	        }
	    }
	        
	    $this->response->type('application/json');
	    $this->response->body(json_encode($response));
	    return $this->response->send();
	}

	public function delete($id){
	    $this->layout = false;
	    $response = array('status'=>'failed', 'message'=>'HTTP method not allowed');
	    
	    if($this->request->is('delete')){
	        if(!empty($id)){
	            if($this->Product->delete($id)){
	                $response = array('status'=>'success','message'=>'Product successfully deleted');
	            }
	        }
	    }
	        
	    $this->response->type('application/json');
	    $this->response->body(json_encode($response));
	    return $this->response->send();
	}
}
?>